import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//rutas
import {app_routing} from './app.routes';

//material
import {
  MatButtonModule,
  MatIconModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatSnackBarModule,
  MatToolbarModule,
  MatTooltipModule,
  MatFormFieldModule
} from "@angular/material";

//primeNG
import {
  CalendarModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  GrowlModule,
  MessagesModule,
  RadioButtonModule,
  SharedModule,
  PanelModule
} from 'primeng/primeng';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { AdmDisponibilidadComponent } from './components/adm-disponibilidad/adm-disponibilidad.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    HomeComponent,
    AdmDisponibilidadComponent
  ],
  imports: [
    app_routing,
    BrowserModule,
    //material
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatTooltipModule,
    MatFormFieldModule
    // //primeNG
    // CalendarModule,
    // DataTableModule,
    // DialogModule,
    // DropdownModule,
    // GrowlModule,
    // MessagesModule,
    // RadioButtonModule,
    // SharedModule,
    // PanelModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
